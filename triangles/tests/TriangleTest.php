<?php
  require_once 'src/Triangle.php';

  class MyTest extends \PHPUnit_Framework_TestCase 
  {
  	/**
  	*	@dataProvider valuesProvider 
  	*/
    public function test_negativeValue($a, $b, $c, $expected)
    {	
		$triangle = new Triangle($a, $b, $c);
		$triangle->setTType();
		$result = $triangle->getTType(); 
     	$this->assertEquals($result, $expected);
    }

  	public function valuesProvider() 
  	{
		return 
		[ 
			[1, 1, 1, "EQUILATERAL"], 
			[2, 1, 2, "ISOCELE"], 
			[3, 4, 5, "QUELCONQUE"],

			[1, -1, 1, "ERROR"], 		// null or negative value
			[0, 1, 1, "ERROR"],			// null or negative value
			[1, 0, 1, "ERROR"],			// null or negative value

			["T", 1, 1, "ERROR"],		// alphabetic char
			[1, "T", 1, "ERROR"],		// alphabetic char
			[1, 1, "T", "ERROR"],		// alphabetic char

			[".", 1, 1, "ERROR"],		// char symbol
			[T, "/", 1, "ERROR"],		// char symbol
			[T, 1, ":", "ERROR"],		// char symbol

			[4, 2, 6, "ERROR"],			// wrong sum
			[4, 9, 5, "ERROR"],			// wrong sum
			[8, 4, 4, "ERROR"],			// wrong sum

		];
    }
}
?>
