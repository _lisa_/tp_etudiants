<?php
	/**
	* 
	*/
	class Triangle
	{
		private $a, $b, $c;

		private $type="unknown";

		function __construct( $a, $b, $c)
		{
			$this->a = $a;
			$this->b = $b;
			$this->c = $c;
		}

		function setError()
		{
			$this->type = "ERROR";
			return false; 	
		}

		function isValid()
		{

			/* error variables are not all integer */
			if( ! is_int($this->a) || ! is_int($this->b) || ! is_int($this->c) )
				return $this->setError();

			/* error if a value is <= 0 */  

			if( $this->getA() <= 0 || $this->getB() <= 0 || $this->getC() <= 0 )
				return $this->setError();
			
			/* put the max value in a */

			if( $this->b > $this->a )
			{
				$tmp = $this->a;
				$this->a  = $this->b;
				$this->b = $tmp;
			}

			if( $this->c > $this->a )
			{
				$tmp = $this->a;
				$this->a  = $this->c;
				$this->c = $tmp;
			}

			/* error if c + b < = a */

			if( $this->b + $this->c <= $this->a )
				return $this->setError();

			return true; 
		} 

		function getTType()
		{
			return $this->type;
		}

		function setTType()
		{ 
			/* ERROR */
		 	if( ! $this->isValid() )
		 		return;  
			
			/* ISO */
			else if( $this->getA() ==  $this->getB() && $this->getA() ==  $this->getC() && $this->getC() ==  $this->getB() )
				$this->type = "EQUILATERAL";

			/* EQUI */
			else if( $this->getA() ==  $this->getB() || $this->getB() == $this->getC() || $this->getC() == $this->getA() )
				$this->type = "ISOCELE";

			/* QUELC */
			else 
				$this->type = "QUELCONQUE";
		}

		function getA() { return $this->a; }
		function getB() { return $this->b; }
		function getC() { return $this->c; }	
	}